package admin.lokacart.ict.mobile.com.adminapp;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import admin.lokacart.ict.mobile.com.adminapp.fragments.DeliveredOrderFragment;

/**
 * Created by Vishesh on 15-01-2016.
 */
public class OrderRecyclerViewAdapter extends RecyclerView.Adapter<OrderRecyclerViewAdapter.DataObjectHolder>
{
    static String LOG_TAG = "OrderRecyclerViewAdapter";
  //  ArrayList<String> productList, productQuantityList, productPriceList;
    Context context;
    List<SavedOrder> orderList = new ArrayList<SavedOrder>();
    Boolean paymentStatus;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
    {
        TextView orderid,username, date,payment;
        MyListener callback;
        public DataObjectHolder(View itemView, Context context)
        {
            super(itemView);
            orderid = (TextView) itemView.findViewById(R.id.order_Id);
            username = (TextView) itemView.findViewById(R.id.username);
            date = (TextView) itemView.findViewById(R.id.date);
            payment=(TextView) itemView.findViewById(R.id.paymentStatus);
            callback = (MyListener) context;
        }
    }

    public OrderRecyclerViewAdapter(ArrayList<SavedOrder> myDataset, Context context,Boolean paymentStatus)
    {
        orderList = myDataset;
        this.context = context;
        this.paymentStatus=paymentStatus;
    }


    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.orders_list_item_card_view, parent, false);
        DataObjectHolder dataObjectHolder = new DataObjectHolder(view, context);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position)
    {
        SavedOrder order = orderList.get(position);
        holder.orderid.setText("" + order.getOrderId());
        holder.username.setText(order.getUserName());

        if(paymentStatus){
            holder.payment.setTextColor(Color.GRAY);
            if(order.getPayment().equals("true")){
                holder.payment.setText("Paid");
            }
            else
            {
                holder.payment.setText("Unpaid");
                holder.payment.setTextColor(Color.RED);
            }

        }else{
            holder.payment.setVisibility(View.GONE);
        }

        Date time = null;
        try {
            time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(order.getTimeStamp());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String date = new SimpleDateFormat("EEE, MMM d, yyyy").format(time);
        holder.date.setText(""+date);
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

}
