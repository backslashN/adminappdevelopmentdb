package admin.lokacart.ict.mobile.com.adminapp.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import admin.lokacart.ict.mobile.com.adminapp.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.Master;
import admin.lokacart.ict.mobile.com.adminapp.MemberRecyclerViewAdapter;
import admin.lokacart.ict.mobile.com.adminapp.OrderRecyclerViewAdapter;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.RecyclerItemClickListener;
import admin.lokacart.ict.mobile.com.adminapp.SavedOrder;

/**
 * Created by madhav on 10/6/16.
 */



public class DeliveredOrderFragment extends Fragment {

    public View deliveredOrderFragmentView;
    private SwipeRefreshLayout swipeContainer;
    public static ArrayList<SavedOrder> deliveredOrderArrayList;
    ArrayList<SavedOrder> orders;
    ArrayList<JSONObject> orderObjects;
    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    protected LayoutManagerType mCurrentLayoutManagerType;
    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }
    TextView tOrders;
    static int recyclerViewIndex;
    private int count=0;
    JSONObject responseObject;
    private int apiCalls=0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        deliveredOrderFragmentView=inflater.inflate(R.layout.orders_recycler_view, container, false);
        getActivity().setTitle(R.string.title_orders);

        swipeContainer = (SwipeRefreshLayout) deliveredOrderFragmentView.findViewById(R.id.swipeRefreshLayout);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Call the API to generate the list
                new GetDeliveredOrderList(false).execute();
            }
        });
        swipeContainer.setColorSchemeResources(
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light,
                android.R.color.holo_blue_bright);


        return deliveredOrderFragmentView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        deliveredOrderArrayList = new ArrayList<SavedOrder>();
    }

    public ArrayList<SavedOrder> getOrders() {
        orders = new ArrayList<SavedOrder>();
        int count=0;
        if(orderObjects.size()!=0)
        {
            for(JSONObject entry : orderObjects){
                SavedOrder order = new SavedOrder(entry,count);
                orders.add(order);
                count++;
            }
        }
        else
        {


        }
        return orders;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        //Call the API to generate the list

            new GetDeliveredOrderList(true).execute();


        mRecyclerView = (RecyclerView) deliveredOrderFragmentView.findViewById(R.id.savedOrderRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        tOrders = (TextView) deliveredOrderFragmentView.findViewById(R.id.tOrder);
        tOrders.setText(R.string.label_no_delivered_orders_present);
        tOrders.setVisibility(View.GONE);

        if (savedInstanceState != null) {
            mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState.getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);
    }


    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        }

        //if(layoutManagerType!=null)
        {
            switch (layoutManagerType) {
                case LINEAR_LAYOUT_MANAGER:
                    mLayoutManager = new LinearLayoutManager(getActivity());
                    mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                    break;
                default:
                    mLayoutManager = new LinearLayoutManager(getActivity());
                    mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
            }


            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.scrollToPosition(scrollPosition);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
            mAdapter = new OrderRecyclerViewAdapter(deliveredOrderArrayList, getActivity(),true);
            mRecyclerView.setAdapter(mAdapter);
            //mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, Master.deliveredOrderKey, DeliveredOrderFragment.this));
            mAdapter.notifyDataSetChanged();

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, mCurrentLayoutManagerType);
        super.onSaveInstanceState(savedInstanceState);
    }

    public void clickListener(int position){

       // System.out.println(processedOrderArrayList.get(position).getPayment());
        new ViewBillAsyncTask(getActivity(),position,deliveredOrderArrayList.get(position).getPayment()).execute("" + deliveredOrderArrayList.get(position).getOrderId());

    }

    //------------------------------------------------API to get the Delivered Order List---------------------------------

    public class GetDeliveredOrderList extends AsyncTask<Void, String, String>
    {
        String response;
        ProgressDialog pd;
        Boolean showProgressDialog;

        public GetDeliveredOrderList(Boolean showProgressDialog)
        {
            this.showProgressDialog = showProgressDialog;
        }

        @Override
        protected void onPreExecute() {
            if(showProgressDialog)
            {
                pd = new ProgressDialog(getActivity());
                pd.setMessage(getString(R.string.label_please_wait));
                pd.setCancelable(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(Void... params)
        {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getDeliveredOrderList(AdminDetails.getAbbr()), null, "GET", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String response)
        {
            if(showProgressDialog)
            {   if(pd != null && pd.isShowing())
                    pd.dismiss();
            }

            if(DeliveredOrderFragment.this.isAdded()){

                if(response.equals("exception")){
                    Master.alertDialog(getActivity(), getString(R.string.label_cannot_connect_to_the_server), "OK");
                }
                else{

                    try {
                        orderObjects = new ArrayList<JSONObject>();
                        responseObject = new JSONObject(response);
                        JSONArray jsonArray = responseObject.getJSONArray("orders");
                        if (jsonArray.length() == 0) {
                            tOrders.setVisibility(View.VISIBLE);
                        } else {
                            tOrders.setVisibility(View.GONE);
                            ++count;
                            for (recyclerViewIndex = 0; recyclerViewIndex < jsonArray.length(); ++recyclerViewIndex) {
                                orderObjects.add((JSONObject) jsonArray.get(recyclerViewIndex));
                            }
                            deliveredOrderArrayList = getOrders();
                            mAdapter = new OrderRecyclerViewAdapter(deliveredOrderArrayList, getActivity(),true);
                            if (count < 2) {
                                mRecyclerView.setAdapter(mAdapter);
                                mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, Master.deliveredOrderKey, DeliveredOrderFragment.this));
                                mAdapter.notifyDataSetChanged();
                            } else {
                                mRecyclerView.swapAdapter(mAdapter, true);
                            }
                        }

                        swipeContainer.setRefreshing(false);

                    } catch (Exception e) {

                    }

                }
            }

        }
    }

    //-----------------------------------------------------------------------------------------------------------------------

    public class ViewBillAsyncTask extends AsyncTask<String,String,String>
    {
        Context context;
        ProgressDialog pd;
        int position;
        String payment;

        ViewBillAsyncTask(Context context,int position,String payment) {
            this.context = context;
            this.position=position;
            this.payment=payment;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String status;
            HttpURLConnection linkConnection = null;
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            String emailid = sharedPreferences.getString("emailid","test2@gmail.com");
            String password =  sharedPreferences.getString("password","password");

            try {

                URL linkurl = new URL("http://ruralict.cse.iitb.ac.in/ruralict/api/"+AdminDetails.getAbbr()+"/viewbill/"+params[0]);
                linkConnection = (HttpURLConnection) linkurl.openConnection();
                String basicAuth = "Basic " + new String(Base64.encode((emailid + ":" + password).getBytes(), Base64.NO_WRAP));
                linkConnection.setRequestProperty("Authorization", basicAuth);
                linkConnection.setDefaultUseCaches(false);
                linkConnection.setRequestMethod("GET");
                linkConnection.setRequestProperty("Accept", "text/html");
                linkConnection.setDoInput(true);
                InputStream is = null;
                status=String.valueOf(linkConnection.getResponseCode());
                if(status.equals("200"))
                {
                    is=linkConnection.getInputStream();
                }
                else
                {
                    status="exception";
                    return  status;
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                status = sb.toString();
            } catch (Exception e) {
                e.printStackTrace();
                status="exception";
                return status;
            }
            finally {
                if (linkConnection != null) {
                    linkConnection.disconnect();
                }
            }
            return status;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(pd != null && pd.isShowing())
                pd.dismiss();


            if(DeliveredOrderFragment.this.isAdded()){
                if (s.equals("exception"))
                {
                    Master.alertDialog(getActivity(), getString(R.string.label_cannot_connect_to_the_server), "OK");
                }
                else
                {
                    final AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    alert.setTitle("Bill");
                    WebView wv = new WebView(context);
                    wv.loadData(s, "text/html", "UTF-8");
                    wv.setWebViewClient(new WebViewClient() {
                        @Override
                        public boolean shouldOverrideUrlLoading(WebView view, String url) {
                            view.loadUrl(url);
                            return true;
                        }
                    });
                    alert.setView(wv);

                    if(payment.equals("false")) {
                        alert.setNegativeButton("Paid? ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                AlertDialog.Builder confirm_alert = new AlertDialog.Builder(context);
                                confirm_alert.setTitle("Are you sure the order is paid ?");
                                confirm_alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        new ChangePaidStateTask(true, position, deliveredOrderArrayList.get(position).getOrderId()).execute();
                                    }
                                });
                                confirm_alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                confirm_alert.show();

                            }
                        });
                    }
                    alert.setPositiveButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
                    alert.show();
                }
            }

        }
    }

    public class ChangePaidStateTask extends AsyncTask<String,String,String> {

        String response;
        ProgressDialog pd;
        Boolean showProgressDialog;
        int position;
        int orderId;

        public ChangePaidStateTask(Boolean showProgressDialog,int position,int orderId) {
            this.showProgressDialog = showProgressDialog;
            this.position=position;
            this.orderId=orderId;
        }

        @Override
        protected void onPreExecute() {
            if (showProgressDialog) {
                pd = new ProgressDialog(getActivity());
                pd.setMessage(getString(R.string.label_please_wait));
                pd.setCancelable(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getChangePaidState(orderId), null, "GET", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            if (showProgressDialog) {
                if (pd != null && pd.isShowing())
                    pd.dismiss();
            }



            if (DeliveredOrderFragment.this.isAdded()) {
                try {
                    responseObject = new JSONObject(response);
                    response = responseObject.getString("response");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(response.equals("exception")){
                    Master.alertDialog(getActivity(), getString(R.string.label_cannot_connect_to_the_server), "OK");
                }
                else if(response.equals("Success")){
                    //Toast.makeText(getActivity(),"Change Successful.Please refresh to see the changes.",Toast.LENGTH_SHORT).show();
                    deliveredOrderArrayList.get(position).setPayment("true");
                    mAdapter.notifyItemChanged(position);
                }
                else {
                    Toast.makeText(getActivity(),R.string.label_toast_something_went_worng,Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

}
