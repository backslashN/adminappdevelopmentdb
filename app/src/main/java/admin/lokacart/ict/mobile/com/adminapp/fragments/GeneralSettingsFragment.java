package admin.lokacart.ict.mobile.com.adminapp.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInput;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import admin.lokacart.ict.mobile.com.adminapp.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.DashboardActivity;
import admin.lokacart.ict.mobile.com.adminapp.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.Master;
import admin.lokacart.ict.mobile.com.adminapp.R;

/**
 * Created by Vishesh on 29-02-2016.
 */

public class GeneralSettingsFragment extends Fragment{

    View generalSettingsFragmentView,editMinimumBillAmount;
    ProgressDialog pd;
    String response;
    JSONObject responseObject;
    Boolean autoApprove, stockManagement;
    Button bSaveSettings;
    TextView tMinimumBillAmount,tAboutOrganisation,tDescriptionCharRemaining;
    EditText eMinimumBillAmount;
    AlertDialog changeMinimumBillAmount;
    Button bPositive,bNegative,bUploadLogo,bCancel,bSave;
    String sMinimumBillAmount;
    String sAmount;
    final int REQUEST_GALLERY = 2, REQUEST_CAMERA = 1, RESULT_OK = -1;
    String tempImage;
    Random randomno;
    private static String capturedImageFilePath, selectedImageFilePath;
    ImageView ivProduct;
    EditText eOrganisationDescription;

    File file, file1;
    float dpHeight, dpWidth;

    //ToggleButton tbAutoApprove, tbStockManagement;
    SwitchCompat sAutoApprove, sStockManagement;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        sMinimumBillAmount="Minimum Bill Amount: Rs.";
        randomno = new Random();


        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        dpWidth = displayMetrics.widthPixels / displayMetrics.density;

        generalSettingsFragmentView = inflater.inflate(R.layout.fragment_general_settings, container, false);
        return generalSettingsFragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("General Settings fag","in onResume");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


       // Log.e("General Settings fag","in onActivityCreated");
     //   bSave = (Button) generalSettingsFragmentView.findViewById(R.id.bGeneralSettingsSave);
      //  bSave.setVisibility(View.GONE);
      //  sAutoApprove = (SwitchCompat) generalSettingsFragmentView.findViewById(R.id.sAutoApprove);
      //  sAutoApprove.setHighlightColor(getResources().getColor(R.color.colorPrimaryDark));
        tMinimumBillAmount=(TextView)generalSettingsFragmentView.findViewById(R.id.tMinimumBillAmount);
        tAboutOrganisation=(TextView) generalSettingsFragmentView.findViewById(R.id.tAboutOrganisation);
        sStockManagement = (SwitchCompat) generalSettingsFragmentView.findViewById(R.id.sStockManagement);
        sStockManagement.setHighlightColor(getResources().getColor(R.color.colorPrimaryDark));

        if(getActivity()!=null && Master.isNetworkAvailable(getActivity())) {
            new GetGeneralSettingsTask().execute();
        }

        tAboutOrganisation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog=new Dialog(getActivity());
                dialog.setContentView(R.layout.about_organisation);
                dialog.setTitle("Modify Organisation Details");
                dialog.show();
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);

                eOrganisationDescription=(EditText) dialog.findViewById(R.id.eOrganisationDescription);
                tDescriptionCharRemaining = (TextView) dialog.findViewById(R.id.tDescriptionCharRemaining);

                eOrganisationDescription.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean hasFocus) {
                        if(!hasFocus){
                            try {
                                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });

                eOrganisationDescription.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void afterTextChanged(Editable s) {}

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        if(s.length() != -1)
                            tDescriptionCharRemaining.setText((500 - s.length()) + " characters left");
                    }
                });

                bUploadLogo=(Button) dialog.findViewById(R.id.bUploadLogo);
                bSaveSettings=(Button) dialog.findViewById(R.id.bSaveSettings);
                bUploadLogo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        uploadImage();
                    }
                });

                bSaveSettings.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String message= eOrganisationDescription.getText().toString().trim();

                        if(message.equals("") || message.isEmpty()){
                            Toast.makeText(getActivity(),getString(R.string.label_toast_organisation_description_request), Toast.LENGTH_SHORT).show();
                        }
                        else if(message.length()> 500){
                            Toast.makeText(getActivity(),getString(R.string.label_toast_organisation_description_length),Toast.LENGTH_SHORT).show();
                        }
                        else if(getActivity()!=null && !Master.isNetworkAvailable(getActivity())){
                            Toast.makeText(getActivity(),R.string.label_toast_no_network_connection,Toast.LENGTH_SHORT).show();
                        }
                        else {
                            JSONObject jsonObject = new JSONObject();
                            try{
                                jsonObject.put("description",eOrganisationDescription.getText().toString());
                                new UploadOrganisationDescriptionTask().execute(jsonObject);

                            }catch (JSONException e){
                                e.printStackTrace();
                            }

                        }
                        dialog.dismiss();
                    }
                });
            }
        });


        tMinimumBillAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
                builder.setCancelable(false);
                editMinimumBillAmount=getActivity().getLayoutInflater().inflate(R.layout.change_min_bill_amount,null);
                builder.setView(editMinimumBillAmount);
                builder.setTitle("Change Minimum Bill Amount");
                builder.setPositiveButton(R.string.label_button_confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                builder.setNegativeButton(R.string.label_button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                eMinimumBillAmount=(EditText) editMinimumBillAmount.findViewById(R.id.eMinimumBillAmount);
                eMinimumBillAmount.setText(sAmount.toString());
                changeMinimumBillAmount=builder.create();
                changeMinimumBillAmount.show();

                bPositive=changeMinimumBillAmount.getButton(AlertDialog.BUTTON_POSITIVE);
                bNegative=changeMinimumBillAmount.getButton(AlertDialog.BUTTON_NEGATIVE);

                bPositive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(!sAmount.equals(eMinimumBillAmount.getText().toString())) {

                            sAmount=eMinimumBillAmount.getText().toString();
                            try {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("orderThreshold", eMinimumBillAmount.getText().toString().trim());
                                new UpdateThresholdTask().execute(jsonObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else{
                            Toast.makeText(getActivity(),R.string.label_toast_no_changes_made,Toast.LENGTH_SHORT).show();
                            changeMinimumBillAmount.dismiss();
                        }

                    }
                });

                bNegative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        changeMinimumBillAmount.dismiss();
                    }
                });

            }

        });



        sStockManagement.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                        @Override
                                                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                            if (getActivity() != null && Master.isNetworkAvailable(getActivity()) && sStockManagement.isPressed()) {
                                                                    JSONObject jsonObject = new JSONObject();
                                                                    try {
                                                                        jsonObject.put("stockManagement", sStockManagement.isChecked());
                                                                        jsonObject.put("autoApprove", true);
                                                                        new UpdateGeneralSettingsTask().execute(jsonObject);
                                                                    } catch (JSONException e) {
                                                                    }
                                                            } else if(sStockManagement.isPressed()) {
                                                                Toast.makeText(getActivity(), R.string.label_toast_no_network_connection, Toast.LENGTH_SHORT).show();
                                                            }

                                                        }
                                                        });

/*        sStockManagement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked= ((SwitchCompat)v).isChecked();

                if (getActivity() != null && Master.isNetworkAvailable(getActivity())) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("stockManagement", checked);
                        jsonObject.put("autoApprove", true);
                        new UpdateGeneralSettingsTask().execute(jsonObject);
                    } catch (JSONException e) {
                    }
                } else {
                    Toast.makeText(getActivity(), R.string.label_toast_no_network_connection, Toast.LENGTH_SHORT).show();
                }


            }
        });*/


//        bSave.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
    }


    private void uploadImage()
    {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    tempImage = "temp" + randomno.nextInt(1000000) + ".jpg";
                    File f = new File(android.os.Environment.getExternalStorageDirectory(), tempImage);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/jpg");
                    startActivityForResult(intent, REQUEST_GALLERY);
                } else {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
        {
            if (requestCode == REQUEST_CAMERA) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals(tempImage)) {
                        f = temp;
                        break;
                    }
                }

                try {
                    final Bitmap bitmap;
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                    //bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);
                    bitmap = Master.readBitmap(Uri.fromFile(f),getActivity(), 2);
                    //bitmap = ImageUtils.decodeSampledBitmap(EditProductActivity.this, Uri.fromFile(f));

                    final Dialog dialog = new Dialog(getActivity());
                    dialog.setContentView(R.layout.image_box);
                    dialog.show();
                    capturedImageFilePath = f.getAbsolutePath();

                    ivProduct = (ImageView) dialog.findViewById(R.id.ivProduct);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) dpWidth, (int) dpWidth);
                    ivProduct.setLayoutParams(layoutParams);
                    ivProduct.setImageBitmap(bitmap);

                    // Master.rotateImage(ivProduct, Master.getImageOrientation(f.getAbsolutePath())
                    //                     , ivProduct.getDrawable().getBounds().width()/2, ivProduct.getDrawable().getBounds().height()/2);

                    TextView imagePath = (TextView) dialog.findViewById(R.id.tImageLink);
                    imagePath.setText(capturedImageFilePath);

                    bSave = (Button) dialog.findViewById(R.id.bImageUpload);
                    bSave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (getActivity()!=null && !Master.isNetworkAvailable(getActivity())) {
                                Toast.makeText(getActivity(), R.string.label_cannot_connect_to_the_internet, Toast.LENGTH_LONG).show();
                            } else {
                               // Call the API to upload the image
                                new UploadLogoTask(selectedImageFilePath, dialog, bitmap,true).execute();
                            }

                        }
                    });

                    bCancel = (Button) dialog.findViewById(R.id.bCancel);
                    bCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            try {
                                file1.delete();
                                file.delete();
                                Master.clearBitmap(bitmap);
                            } catch (Exception e) {}
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            else if (requestCode == REQUEST_GALLERY)
            {
                Uri selectedImageURI = data.getData();
                if(selectedImageURI != null) {
                    try {
                        selectedImageFilePath = Master.getPath(getActivity(), selectedImageURI);

                        //Bitmap thumbnail = (BitmapFactory.decodeFile(selectedImageFilePath));

                        final Bitmap bitmap = Master.readBitmap(selectedImageURI,getActivity(), 2);

                        final Dialog dialog = new Dialog(getActivity());
                        dialog.setContentView(R.layout.image_box);
                        dialog.show();

                        ivProduct = (ImageView) dialog.findViewById(R.id.ivProduct);
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) dpWidth, (int) dpWidth);
                        ivProduct.setLayoutParams(layoutParams);
                        ivProduct.setImageBitmap(bitmap);

                        TextView imagePath = (TextView) dialog.findViewById(R.id.tImageLink);
                        imagePath.setText(selectedImageFilePath);

                        bSave = (Button) dialog.findViewById(R.id.bImageUpload);
                        bSave.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (getActivity()!=null && !Master.isNetworkAvailable(getActivity())) {
                                    Toast.makeText(getActivity(), R.string.label_cannot_connect_to_the_internet, Toast.LENGTH_LONG).show();
                                } else {
                                    // Call the API to upload image
                                    new UploadLogoTask(selectedImageFilePath, dialog, bitmap,false).execute();
                                }

                            }
                        });

                        bCancel = (Button) dialog.findViewById(R.id.bCancel);
                        bCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                try {
                                    file1.delete();
                                    Master.clearBitmap(bitmap);
                                } catch (Exception e) {

                                }
                            }
                        });
                        ivProduct.setImageBitmap(bitmap);
                        ivProduct.setVisibility(View.VISIBLE);

                    } catch (Exception e) {
                        Toast.makeText(getActivity(), R.string.label_toast_unable_to_get_the_file_path, Toast.LENGTH_LONG).show();
                    }
                }
            }

        }
    }


    public class GetGeneralSettingsTask extends AsyncTask<String, String, String>
    {
        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getGeneralSettingsURL(AdminDetails.getAbbr(), AdminDetails.getMobileNumber()),
                    null, "GET", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String s) {

            if(pd != null && pd.isShowing())
            pd.dismiss();

            if(GeneralSettingsFragment.this.isAdded()){
                try
                {
                    responseObject = new JSONObject(response);
                    if(responseObject.get("response").equals("success"))
                    {
                        autoApprove = responseObject.getBoolean("autoApprove");
                        //sAutoApprove.setChecked(autoApprove);
                        stockManagement = responseObject.getBoolean("stockManagement");
                        sStockManagement.setChecked(stockManagement);
                        sAmount=responseObject.getString("orderThreshold");
                        tMinimumBillAmount.setText(sMinimumBillAmount + sAmount);

                    }
                    else
                    {
                        Toast.makeText(getActivity(), R.string.label_cannot_connect_to_the_server, Toast.LENGTH_SHORT).show();
                    }
                }
                catch (JSONException e)
                {
                    Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                }
            }

        }
    }

    public class UpdateGeneralSettingsTask extends AsyncTask<JSONObject, String, String>
    {
        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getGeneralSettingsUpdateURL(AdminDetails.getAbbr()),
                    params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            if(pd != null && pd.isShowing()) {
                pd.dismiss();
            }

            try
            {
                responseObject = new JSONObject(response);
                if(responseObject.get("response").equals("Successfully updated"))
                {
                    Toast.makeText(getActivity(), R.string.label_toast_changes_saved_successfully, Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(getActivity(), R.string.label_cannot_connect_to_the_server, Toast.LENGTH_SHORT).show();
                }
            }
            catch (JSONException e)
            {
                Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
            }
        }
    }


    public class UpdateThresholdTask extends AsyncTask<JSONObject,String,String>
    {
        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getThresholdUpdate(AdminDetails.getAbbr()),
                    params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }
        @Override
        protected void onPostExecute(String s) {

            if(pd != null && pd.isShowing())
                pd.dismiss();

            try
            {
                responseObject = new JSONObject(response);
                if(responseObject.get("response").equals("Successfully updated"))
                {
                    Toast.makeText(getActivity(), R.string.label_toast_changes_saved_successfully, Toast.LENGTH_SHORT).show();
                    tMinimumBillAmount.setText(sMinimumBillAmount + eMinimumBillAmount.getText().toString().trim());
                    changeMinimumBillAmount.dismiss();
                }
                else
                {
                    Toast.makeText(getActivity(), R.string.label_cannot_connect_to_the_server, Toast.LENGTH_SHORT).show();
                }
            }
            catch (JSONException e)
            {
                Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
            }
        }
    }

    //---------------------------------------Upload Organisation Description API--------------------------------------------

    public class UploadOrganisationDescriptionTask extends AsyncTask<JSONObject,String,String>
    {
        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getGeneralSettingsOrganisationDescriptionURL(AdminDetails.getAbbr()),
                    params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());

            //System.out.println(response.toString());

            return response;
        }
        @Override
        protected void onPostExecute(String s) {

            if(pd != null && pd.isShowing())
                pd.dismiss();

            try
            {
                responseObject = new JSONObject(response);
                if(responseObject.get("response").equals("Success"))
                {
                    Toast.makeText(getActivity(), R.string.label_toast_changes_saved_successfully, Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(getActivity(), R.string.label_cannot_connect_to_the_server, Toast.LENGTH_SHORT).show();
                }
            }
            catch (JSONException e)
            {
                Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
            }
        }
    }

    //------------------------------- Asynctask to upload Logo ----------------------------------------------

    class UploadLogoTask extends AsyncTask<Void, String, String>
    {
        ProgressDialog pd;
        String path;
        Dialog dialog;
        Bitmap bitmap;
        Boolean isCaptured;
        String imagePath;

        UploadLogoTask(String path, Dialog dialog, Bitmap bitmap,Boolean isCaptured)
        {
            this.bitmap = bitmap;
            this.path = path;
            this.dialog = dialog;
            this.isCaptured=isCaptured;
        }
        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.pd_uploading_file));
            pd.setCancelable(false);
            pd.show();

            System.out.println(path);

            file = new File(path);

            //System.out.println(file.getAbsolutePath());
            // System.out.println(file.getParent());

            imagePath = file.getParent() + "/" + "myImage" + randomno.nextInt(1000000) + ".jpg";


            file1 = new File(imagePath);
            try
            {
                Master.copyFile(file, file1);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            String response;
            response = Master.okhttpUpload(file1, Master.getGeneralSettingsLogoUploadURL(AdminDetails.getAbbr()),
                    Master.IMAGE_FILE_TYPE, AdminDetails.getEmail(), AdminDetails.getPassword());

            System.out.println("Logo URL" + Master.getGeneralSettingsLogoUploadURL(AdminDetails.getAbbr()));
            return response;
        }

        @Override
        protected void onPostExecute(String message) {

            if(pd != null && pd.isShowing())
                pd.dismiss();

            try
            {
                JSONObject responseObject = new JSONObject(message);
                message=responseObject.getString("response");
                System.out.println("message"+message.toString());

                if(message.equals("Image upload successful"))
                {
                    Toast.makeText(getActivity(),
                            R.string.label_toast_image_has_been_uploaded_successfully, Toast.LENGTH_LONG).show();
                    Master.clearBitmap(bitmap);
                    dialog.dismiss();
                }
                else
                {
                    Toast.makeText(getActivity(),
                            R.string.label_cannot_connect_to_the_server, Toast.LENGTH_LONG).show();

                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Toast.makeText(getActivity(),
                        R.string.label_toast_something_went_worng, Toast.LENGTH_LONG).show();
            }
        }
    }
//---------------------End of Upload Image Asynctask-------------------------------------------------

}
