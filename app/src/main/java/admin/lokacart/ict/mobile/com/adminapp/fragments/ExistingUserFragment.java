package admin.lokacart.ict.mobile.com.adminapp.fragments;

import android.animation.ValueAnimator;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.widget.SwipeRefreshLayout;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import admin.lokacart.ict.mobile.com.adminapp.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.Master;
import admin.lokacart.ict.mobile.com.adminapp.Member;
import admin.lokacart.ict.mobile.com.adminapp.MemberRecyclerViewAdapter;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.RecyclerItemClickListener;

/**
 * Created by Vishesh on 08-02-2016.
 */
public class ExistingUserFragment extends Fragment {

    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    protected LayoutManagerType mCurrentLayoutManagerType;
    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }
    ProgressDialog pd;
    public static ArrayList<Member> memberArrayList, members;
    ArrayList<JSONObject> memberObjects;
    private SwipeRefreshLayout swipeContainer;
    RecyclerView mRecyclerView;
    static MemberRecyclerViewAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    JSONObject responseObject;
    static int recyclerViewIndex;
    View existingUserFragmentView;
    Dialog dialog;
    String response, memberName, memberLName, memberEmail, memberAddress, memberPhone, memberPincode;
    private final int ADD_KEY = 0, EDIT_KEY = 1;
    private int count = 0;
    TextView tMember;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        if(ExistingUserFragment.this.isAdded()){
            UnClickChangeColor();
        }

        existingUserFragmentView = inflater.inflate(R.layout.fragment_member_recycler_view, container, false);
        getActivity().setTitle(R.string.title_members);
        setHasOptionsMenu(true);
        tMember = (TextView) existingUserFragmentView.findViewById(R.id.tMember);
        tMember.setText(R.string.label_no_member_present);
        tMember.setVisibility(View.GONE);
        memberArrayList = new ArrayList<Member>();
        swipeContainer = (SwipeRefreshLayout) existingUserFragmentView.findViewById(R.id.memberSwipeToRefreshLayout);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new GetExistingMembersTask(false).execute();
            }
        });
        swipeContainer.setColorSchemeResources(
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light,
                android.R.color.holo_blue_bright);
        return existingUserFragmentView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    public ArrayList<Member> getMembers() {

        members = new ArrayList<Member>();
        if(memberObjects.size()!=0)
        {
            for(JSONObject entry : memberObjects){
                Member member = new Member(entry);
                members.add(member);
            }
        }
        else
        {

        }
        return members;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        mRecyclerView = (RecyclerView) existingUserFragmentView.findViewById(R.id.memberRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        new GetExistingMembersTask(true).execute();
        if (savedInstanceState != null) {
            mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);
    }


    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        if(layoutManagerType!=null){
            switch (layoutManagerType)
            {

                case LINEAR_LAYOUT_MANAGER:
                    mLayoutManager = new LinearLayoutManager(getActivity());
                    mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                    break;
                default:
                    mLayoutManager = new LinearLayoutManager(getActivity());
                    mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
            }
        }


        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }

    public void addNewMember()
    {
        dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.member_box);
        dialog.setTitle(R.string.dialog_title_add_member);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final EditText eName, eLName, eEmail, ePhone, eAddress, ePincode;
        final Button bEdit, bConfirm, bCancel,bDelete;
        final CheckBox cAdmin, cPublisher, cMember;
        final TextView tRole;

        tRole = (TextView) dialog.findViewById(R.id.tRole);
        tRole.setVisibility(View.GONE);

        eName = (EditText) dialog.findViewById(R.id.eMemberFirstName);
        eLName = (EditText) dialog.findViewById(R.id.eMemberLastName);
        eEmail = (EditText) dialog.findViewById(R.id.eMemberEmail);
        ePhone = (EditText) dialog.findViewById(R.id.eMemberPhone);
        eAddress = (EditText) dialog.findViewById(R.id.eMemberAddress);
        ePincode = (EditText) dialog.findViewById(R.id.eMemberPincode);

        cAdmin = (CheckBox) dialog.findViewById(R.id.cAdmin);
        cAdmin.setVisibility(View.GONE);

        cPublisher = (CheckBox) dialog.findViewById(R.id.cPublisher);
        cPublisher.setVisibility(View.GONE);

        cMember = (CheckBox) dialog.findViewById(R.id.cMember);
        cMember.setVisibility(View.GONE);

        bEdit = (Button) dialog.findViewById(R.id.bMemberEdit);
        bEdit.setVisibility(View.GONE);

        bDelete=(Button) dialog.findViewById(R.id.bMemberDelete);
        bDelete.setVisibility(View.GONE);

        bConfirm = (Button) dialog.findViewById(R.id.bMemberConfirm);
        bCancel = (Button) dialog.findViewById(R.id.bMemberCancel);

        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        bConfirm.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //TODO send the new product name
                memberName = eName.getText().toString().trim();
                memberLName = eLName.getText().toString().trim();
                memberPhone = ePhone.getText().toString().trim();
                memberEmail = eEmail.getText().toString().trim();
                memberAddress = eAddress.getText().toString().trim();
                memberPincode = ePincode.getText().toString().trim();

                if (memberEmail.equals("") || memberPhone.equals(""))
                {
                    Toast.makeText(getActivity(), R.string.label_toast_all_fields_are_mandatory, Toast.LENGTH_SHORT).show();
                }
                else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(memberEmail).matches())
                {
                    Toast.makeText(getActivity(), R.string.label_toast_please_enter_a_valid_email_id, Toast.LENGTH_SHORT).show();
                }
                else if(memberPhone.length() != 10)
                {
                    Toast.makeText(getActivity(), R.string.label_toast_please_enter_a_valid_mobile_number, Toast.LENGTH_SHORT).show();
                }
                else if(memberPincode.length() != 6 && memberPincode.length() != 0)
                {
                    Toast.makeText(getActivity(), R.string.label_toast_please_enter_a_valid_pincode, Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Member tempMember;
                    JSONObject jsonObject = new JSONObject();
                    try
                    {
                        jsonObject.put("phonenumber", "91" + memberPhone);
                        jsonObject.put("email", memberEmail);
                        jsonObject.put("abbr", AdminDetails.getAbbr());
                        jsonObject.put("refemail",AdminDetails.getEmail());
                        jsonObject.put("address", memberAddress);
                        jsonObject.put("name",memberName);
                        jsonObject.put("lastname", memberLName);
                        jsonObject.put("pincode", memberPincode);

//                        if(cAdmin.isChecked())
//                            jsonObject.put("isAdmin", "1");
//                        else
//                            jsonObject.put("isAdmin", "0");
//                        if(cPublisher.isChecked())
//                            jsonObject.put("isPublisher", "1");
//                        else
//                            jsonObject.put("isPublisher", "0");

                        tempMember = new Member(jsonObject, 0);
                        new AddEditMemberTask(tempMember, dialog, ADD_KEY, 0).execute(jsonObject);
                    }
                    catch (Exception e)
                    {
                    }
                }
            }
        });
    }

    public void clickListener(final int position) // add a new member
    {
        dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.member_box);
        dialog.setTitle("Edit details of " + memberArrayList.get(position).getName());
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final EditText eName, eLName, eEmail, ePhone, eAddress, ePincode;
        final Button bEdit, bConfirm, bCancel,bDelete;
        final CheckBox cAdmin, cPublisher, cMember;
        final TextView tRole;

        tRole = (TextView) dialog.findViewById(R.id.tRole);
        tRole.setVisibility(View.GONE);

        eName = (EditText) dialog.findViewById(R.id.eMemberFirstName);
        eName.setText(memberArrayList.get(position).getName());
        eName.setEnabled(false);

        eLName = (EditText) dialog.findViewById(R.id.eMemberLastName);
        eLName.setText(memberArrayList.get(position).getLastName());
        eLName.setEnabled(false);

        eEmail = (EditText) dialog.findViewById(R.id.eMemberEmail);
        eEmail.setText(memberArrayList.get(position).getEmail());
        eEmail.setEnabled(false);

        ePhone = (EditText) dialog.findViewById(R.id.eMemberPhone);
        ePhone.setText(memberArrayList.get(position).getPhone());
        ePhone.setEnabled(false);

        eAddress = (EditText) dialog.findViewById(R.id.eMemberAddress);
        eAddress.setText(memberArrayList.get(position).getAddress());
        eAddress.setEnabled(false);

        ePincode = (EditText) dialog.findViewById(R.id.eMemberPincode);
        ePincode.setText(memberArrayList.get(position).getPincode());
        ePincode.setEnabled(false);

        cAdmin = (CheckBox) dialog.findViewById(R.id.cAdmin);
        cAdmin.setVisibility(View.GONE);

        cPublisher = (CheckBox) dialog.findViewById(R.id.cPublisher);
        cPublisher.setVisibility(View.GONE);

        cMember = (CheckBox) dialog.findViewById(R.id.cMember);
        cMember.setVisibility(View.GONE);

        /*cAdmin = (CheckBox) dialog.findViewById(R.id.cAdmin);
        if(memberArrayList.get(position).isA())
            cAdmin.setChecked(true);
        else
            cAdmin.setChecked(false);
        cAdmin.setEnabled(false);

        cPublisher = (CheckBox) dialog.findViewById(R.id.cPublisher);
        if(memberArrayList.get(position).isP())
            cPublisher.setChecked(true);
        else
            cPublisher.setChecked(false);
        cPublisher.setEnabled(false);*/

        bEdit = (Button) dialog.findViewById(R.id.bMemberEdit);
        bConfirm = (Button) dialog.findViewById(R.id.bMemberConfirm);
        bConfirm.setVisibility(View.GONE);

        bDelete=(Button) dialog.findViewById(R.id.bMemberDelete);

        bCancel = (Button) dialog.findViewById(R.id.bMemberCancel);

        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        bDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog_new=new Dialog(getActivity());
                dialog_new.setContentView(R.layout.delete_confirmation);
                dialog_new.setTitle("Delete " + memberArrayList.get(position).getName() + " ?");
                dialog_new.show();
                dialog_new.setCanceledOnTouchOutside(true);
                dialog_new.setCancelable(true);

                final Button button_confirm= (Button) dialog_new.findViewById(R.id.confirm_button);
                final Button button_cancel = (Button) dialog_new.findViewById(R.id.cancel_button);

                button_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_new.dismiss();
                    }
                });

                button_confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // make the delete API call over here
                        if(getActivity()!=null && Master.isNetworkAvailable(getActivity())){
                            JSONObject jsonObject = new JSONObject();

                            try {
                                jsonObject.put("abbr",AdminDetails.getAbbr());
                                jsonObject.put("phonenumber", "91" + memberArrayList.get(position).getPhone());

                                new DeleteMember(position).execute(jsonObject);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            dialog_new.dismiss();
                            dialog.dismiss();
                        }else{
                            Toast.makeText(getActivity(),R.string.label_toast_no_network_connection,Toast.LENGTH_SHORT).show();
                        }



                    }
                });


            }
        });

        bEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eName.setEnabled(true);
                eLName.setEnabled(true);
                eAddress.setEnabled(true);
               // ePhone.setEnabled(true);
                ePincode.setEnabled(true);
                cPublisher.setEnabled(true);
                cAdmin.setEnabled(true);

                bEdit.setVisibility(View.GONE);
                bConfirm.setVisibility(View.VISIBLE);
            }
        });

        bConfirm.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //TODO send the new product name
                memberName = eName.getText().toString().trim();
                memberLName = eLName.getText().toString().trim();
                memberPhone = ePhone.getText().toString().trim();
                memberEmail = eEmail.getText().toString().trim();
                memberAddress = eAddress.getText().toString().trim();
                memberPincode = ePincode.getText().toString().trim();

                if (memberName.equals("") || memberLName.equals("") || memberAddress.equals("") || memberEmail.equals("") || memberPhone.equals("") || memberPincode.equals(""))
                {
                    Toast.makeText(getActivity(), R.string.label_toast_all_fields_are_mandatory, Toast.LENGTH_SHORT).show();
                }
                else if(memberPhone.length() != 10)
                {
                    Toast.makeText(getActivity(), R.string.label_toast_please_enter_a_valid_mobile_number, Toast.LENGTH_SHORT).show();
                }
                else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(memberEmail).matches())
                {
                    Toast.makeText(getActivity(), R.string.label_toast_please_enter_a_valid_email_id, Toast.LENGTH_SHORT).show();
                }
                else if(memberPincode.length() != 6)
                {
                    Toast.makeText(getActivity(), R.string.label_toast_please_enter_a_valid_pincode, Toast.LENGTH_SHORT).show();
                }
                else if(memberName.equals(memberArrayList.get(position).getName())
                        && memberLName.equals(memberArrayList.get(position).getLastName())
                        && memberEmail.equals(memberArrayList.get(position).getEmail())
                        && memberPhone.equals(memberArrayList.get(position).getPhone())
                        && memberAddress.equals(memberArrayList.get(position).getAddress())
                        && memberPincode.equals(memberArrayList.get(position).getPincode()))
                {
                    Toast.makeText(getActivity(), R.string.label_toast_no_changes_to_save, Toast.LENGTH_SHORT).show();
                }
                else if(getActivity()!=null && Master.isNetworkAvailable(getActivity()))
                {
                    Member tempMember;
                    JSONObject jsonObject = new JSONObject();
                    try
                    {
                        jsonObject.put("name", memberName);
                        jsonObject.put("lastname", memberLName);
                        jsonObject.put("userid", memberArrayList.get(position).getUserID());
                        jsonObject.put("email", memberEmail);
                        jsonObject.put("phone", "91" + memberPhone);
                        jsonObject.put("address", memberAddress);
                        jsonObject.put("pincode", memberPincode);
//                        if(cAdmin.isChecked())
//                            jsonObject.put("isAdmin", "1");
//                        else
//                            jsonObject.put("isAdmin", "0");
//                        if(cPublisher.isChecked())
//                            jsonObject.put("isPublisher", "1");
//                        else
//                            jsonObject.put("isPublisher", "0");

                        tempMember = new Member(jsonObject, 0);
                        tempMember.setPincode(memberPincode);
                        tempMember.setUserID(memberArrayList.get(position).getIntUserID());
                        new AddEditMemberTask(tempMember, dialog, EDIT_KEY, position).execute(jsonObject);
                    }
                    catch (Exception e)
                    {
                    }
                }
                else{
                    Toast.makeText(getActivity(),R.string.label_toast_no_network_connection,Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public class GetExistingMembersTask extends AsyncTask<Void, String, String>
    {
        ProgressDialog pd;
        Boolean showProgressDialog;

        public GetExistingMembersTask(Boolean showProgressDialog)
        {
            this.showProgressDialog = showProgressDialog;
        }

        @Override
        protected void onPreExecute() {
            if(showProgressDialog)
            {
                pd = new ProgressDialog(getActivity());
                pd.setMessage(getString(R.string.label_please_wait));
                pd.setCancelable(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(Void... params)
        {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getExistingUsersURL(AdminDetails.getAbbr()), null, "GET", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String message)
        {
            if(showProgressDialog)
            {
                if(pd != null && pd.isShowing())
                    pd.dismiss();
            }

            if(ExistingUserFragment.this.isAdded()){
                if (response.equals("exception"))
                {
                    Master.alertDialog(getActivity(), getString(R.string.label_cannot_connect_to_the_server), "OK");
                }
                else
                {
                    try
                    {

                        memberObjects= new ArrayList<JSONObject>();
                        responseObject = new JSONObject(response);
                        JSONArray jsonArray = responseObject.getJSONArray("users");
                        if(jsonArray.length() == 0)
                        {
                            tMember.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            count++;
                            tMember.setVisibility(View.GONE);
                            for (recyclerViewIndex = 0; recyclerViewIndex < jsonArray.length(); ++recyclerViewIndex)
                            {
                                memberObjects.add((JSONObject)jsonArray.get(recyclerViewIndex));
                            }
                            memberArrayList = getMembers();
                            mAdapter = new MemberRecyclerViewAdapter(memberArrayList, getActivity());
                            if(count < 2)
                            {
                                mRecyclerView.setAdapter(mAdapter);
                                mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, Master.existingUserClickKey, ExistingUserFragment.this));
                                mAdapter.notifyDataSetChanged();
                            }
                            else
                            {
                                mRecyclerView.swapAdapter(mAdapter,true);
                            }
                        }
                        swipeContainer.setRefreshing(false);
                    }
                    catch (Exception e)
                    {
                    }
                }
            }

        }
    }

    private class AddEditMemberTask extends AsyncTask<JSONObject, String, String>
    {
        Member member;
        Dialog d;
        int category, position;
        public AddEditMemberTask(Member member, Dialog dialog, int category, int position)
        {
            d = dialog;
            this.member = member;
            if(category == EDIT_KEY)
            {
                this.position = position;
                this.category = category;
            }
            else
                this.category = ADD_KEY;
        }
        @Override
        protected void onPreExecute()
        {
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            //System.out.println("Add member URL: " + Master.getAddUserURL());
            //System.out.println("Add member JSON: " + params[0]);
            if(category == ADD_KEY)
                response = getJSON.getJSONFromUrl(Master.getAddUserURL(), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            else
                response = getJSON.getJSONFromUrl(Master.getEditUserURL(AdminDetails.getAbbr()), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());

            //System.out.println("Add member response: " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String message)
        {

            if(ExistingUserFragment.this.isAdded()){
                if(message.equals("exception"))
                {
                    Toast.makeText(getActivity(), R.string.label_cannot_connect_to_the_server, Toast.LENGTH_SHORT).show();
                }
                else
                {
                    try
                    {
                        responseObject = new JSONObject(message);
                        if(responseObject.getString("response").equals("success"))
                        {
                            if(category == ADD_KEY)
                            {
                                //System.out.println("In addition of add key");
                                //member.setUserID(responseObject.getInt("userId"));
//                            memberArrayList.add(recyclerViewIndex, member);
//                            mAdapter.notifyItemInserted(recyclerViewIndex++);
                                dialog.dismiss();
                                Toast.makeText(getActivity(), R.string.label_toast_member_added_successfully, Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                memberArrayList.set(position, member);
                                mAdapter.notifyItemChanged(position);
                                dialog.dismiss();
                                Toast.makeText(getActivity(), R.string.label_toast_member_details_successfully_updated, Toast.LENGTH_SHORT).show();
                            }
                        }
                        else if(responseObject.getString("response").equals("Email ID already exists"))
                        {
                            Toast.makeText(getActivity(), R.string.label_toast_email_id_already_exists, Toast.LENGTH_SHORT).show();
                        }
                        else if(responseObject.getString("response").equals("Phone Number already exists"))
                        {
                            Toast.makeText(getActivity(), R.string.label_toast_phone_number_already_exists, Toast.LENGTH_SHORT).show();
                        }
                        else if(responseObject.getString("response").equals("Already a member")){
                            Toast.makeText(getActivity(),R.string.label_toast_already_a_member,Toast.LENGTH_SHORT).show();
                        }
                        else if(responseObject.getString("response").equals("failure")){

                            try{
                                if(responseObject.getString("reason").equals("Admin accounts cannot be used on the consumer app")){
                                    Toast.makeText(getActivity(),R.string.label_toast_admin_acc_not_on_client_app,Toast.LENGTH_SHORT);
                                }
                            }catch (JSONException e){

                            }
                        }
                        else if(responseObject.getString("response").equals("Email ID and Phone number mismatch")){


                            Toast.makeText(getActivity(), R.string.label_toast_email_phone_mismatch, Toast.LENGTH_SHORT).show();

                        }
                        else
                        {
                            Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }

            if(pd != null && pd.isShowing()){
                pd.dismiss();
            }

        }
    }

    private  class DeleteMember extends AsyncTask<JSONObject,String,String >{

        ProgressDialog pd;
        int position;

        public DeleteMember(int position){
            this.position=position;

        }

        @Override
        protected void onPreExecute() {
            pd=new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();

        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getDeleteUser(), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String response) {

            pd.dismiss();

            try {
                responseObject = new JSONObject(response);
                response = responseObject.getString("response");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (!Master.isNetworkAvailable(getActivity())) {
                Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection + "", Toast.LENGTH_SHORT);
            } else if (response.equals("exception")) {
                Master.alertDialog(getActivity(), "Cannot connect to the server", "OK");
            } else if (response.equals("Cannot be deleted")) {
                Toast.makeText(getActivity(), R.string.label_toast_cannot_delete_member, Toast.LENGTH_SHORT).show();
            } else if (response.equals("Membership removed")) {
                Toast.makeText(getActivity(), R.string.label_toast_member_removed, Toast.LENGTH_SHORT).show();
                memberArrayList.remove(position);
                mAdapter.notifyItemRemoved(position);
                recyclerViewIndex--;
            }
            else{
                Toast.makeText(getActivity(), R.string.label_toast_error, Toast.LENGTH_SHORT).show();
            }

            }


        }

    private void UnClickChangeColor() {
        // Initial colors of each system bar.
        final int statusBarColor = getResources().getColor(R.color.toColorPrimary);
        final int toolbarColor = getResources().getColor(R.color.toColorPrimaryDark);

        // Desired final colors of each bar.
        final int statusBarToColor = getResources().getColor(R.color.colorPrimary);
        final int toolbarToColor = getResources().getColor(R.color.colorPrimaryDark);

        ValueAnimator anim = ValueAnimator.ofFloat(0, 1);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // Use animation position to blend colors.
                float position = animation.getAnimatedFraction();

                // Apply blended color to the status bar.
                int blended = blendColors(statusBarColor, statusBarToColor, position);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && getActivity() != null) {
                    getActivity().getWindow().setStatusBarColor(blended);
                }

                // Apply blended color to the ActionBar.
                blended = blendColors(toolbarColor, toolbarToColor, position);
                ColorDrawable background = new ColorDrawable(blended);
                if(getActivity() != null){
                    ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(background);
                }
            }
        });

        anim.setDuration(150).start();
    }



    private int blendColors(int from, int to, float ratio) {
        final float inverseRatio = 1f - ratio;

        final float r = Color.red(to) * ratio + Color.red(from) * inverseRatio;
        final float g = Color.green(to) * ratio + Color.green(from) * inverseRatio;
        final float b = Color.blue(to) * ratio + Color.blue(from) * inverseRatio;

        return Color.rgb((int) r, (int) g, (int) b);
    }
}

