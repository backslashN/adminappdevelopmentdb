package admin.lokacart.ict.mobile.com.adminapp.fragments;

import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import admin.lokacart.ict.mobile.com.adminapp.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.Master;
import admin.lokacart.ict.mobile.com.adminapp.R;

/**
 * Created by madhav on 6/6/16.
 */
public class BroadcastMessage extends Fragment {

    View broadcastMessageView;

    public EditText eBroadcastEditText;
    public Button bBroadcastButton;
    public TextView tCharRemaining;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        broadcastMessageView = inflater.inflate(R.layout.fragment_broadcast_message,container,false);
        
        getActivity().setTitle("Broadcast Notification");

        setHasOptionsMenu(true);

        eBroadcastEditText = (EditText) broadcastMessageView.findViewById(R.id.eBroadcastMessage);
        bBroadcastButton = (Button) broadcastMessageView.findViewById(R.id.bBroadcastSubmit);
        tCharRemaining = (TextView) broadcastMessageView.findViewById(R.id.tCharRemaining);

        if(BroadcastMessage.this.isAdded()){
            UnClickChangeColor();
        }


        eBroadcastEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(!hasFocus){
                    try {
                        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        eBroadcastEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != -1)
                    tCharRemaining.setText((100 - s.length()) + " characters left");
            }
        });

        bBroadcastButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Close the keyboard
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(broadcastMessageView.getWindowToken(),0);

                String message= eBroadcastEditText.getText().toString().trim();

                if(message.equals("") || message.isEmpty()){
                    Toast.makeText(getActivity(),getString(R.string.label_toast_broadcast_message_request), Toast.LENGTH_SHORT).show();
                }
                else if(message.length()> 100){
                    Toast.makeText(getActivity(),getString(R.string.label_toast_reduce_message_length),Toast.LENGTH_SHORT).show();
                }
                else if(getActivity()!=null && !Master.isNetworkAvailable(getActivity())){
                    Toast.makeText(getActivity(),R.string.label_toast_no_network_connection,Toast.LENGTH_SHORT).show();
                }
                else {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("orgabbr", AdminDetails.getAbbr());
                        jsonObject.put("message", message);
                    } catch (JSONException e) {
                    }
                    new SendNotification(getActivity()).execute(jsonObject);

                }
            }
        });

        return broadcastMessageView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }



  /*  ------------------------------------API to send notification---------------------------------*/

    public class SendNotification extends AsyncTask<JSONObject,String,String>{

        ProgressDialog progressDialog;
        Context context;
        Master master;
        String response;

        SendNotification(Context context){
            this.context=context;
        }
        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(context.getString(R.string.label_please_wait));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            master= new Master();
            GetJSON getJSON = new GetJSON();
            response=getJSON.getJSONFromUrl(master.getBroadcastURL(),(JSONObject) params[0],"POST",true, AdminDetails.getEmail(),AdminDetails.getPassword());
            return response;

        }

        @Override
        protected void onPostExecute(String response) {

            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();

            if (BroadcastMessage.this.isAdded()) {

                try {
                    JSONObject responseObject = new JSONObject(response);
                    response = responseObject.getString("response");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (response.equals("Message successfully broadcasted")) {
                    Toast.makeText(getActivity(), response.toString(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.label_toast_error), Toast.LENGTH_SHORT).show();
                }

            }
        }


    }


    private void UnClickChangeColor() {
        // Initial colors of each system bar.
        final int statusBarColor = getResources().getColor(R.color.toColorPrimary);
        final int toolbarColor = getResources().getColor(R.color.toColorPrimaryDark);

        // Desired final colors of each bar.
        final int statusBarToColor = getResources().getColor(R.color.colorPrimary);
        final int toolbarToColor = getResources().getColor(R.color.colorPrimaryDark);

        ValueAnimator anim = ValueAnimator.ofFloat(0, 1);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // Use animation position to blend colors.
                float position = animation.getAnimatedFraction();

                // Apply blended color to the status bar.
                int blended = blendColors(statusBarColor, statusBarToColor, position);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && getActivity() != null) {
                    getActivity().getWindow().setStatusBarColor(blended);
                }

                // Apply blended color to the ActionBar.
                blended = blendColors(toolbarColor, toolbarToColor, position);
                ColorDrawable background = new ColorDrawable(blended);
                if(getActivity() != null){
                    ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(background);
                }
            }
        });

        anim.setDuration(150).start();
    }



    private int blendColors(int from, int to, float ratio) {
        final float inverseRatio = 1f - ratio;

        final float r = Color.red(to) * ratio + Color.red(from) * inverseRatio;
        final float g = Color.green(to) * ratio + Color.green(from) * inverseRatio;
        final float b = Color.blue(to) * ratio + Color.blue(from) * inverseRatio;

        return Color.rgb((int) r, (int) g, (int) b);
    }

}
