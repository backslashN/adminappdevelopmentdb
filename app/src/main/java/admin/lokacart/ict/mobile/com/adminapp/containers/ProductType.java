package admin.lokacart.ict.mobile.com.adminapp.containers;

import java.util.ArrayList;

/**
 * Created by madhav on 16/6/16.
 */
public class ProductType {

    public String name;
    public ArrayList<Product> productItems = new ArrayList<>();

    public ProductType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}