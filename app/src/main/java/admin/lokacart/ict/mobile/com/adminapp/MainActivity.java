package admin.lokacart.ict.mobile.com.adminapp;

/**
 * Created by Vishesh on 29/12/15.
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.*;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.app.ProgressDialog;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    LoginVerification loginVerification;
    GetOTPTask anp;
    OTPCountDownTimer countDownTimer;
    AddPasswordTask addPasswordTask;

    Dialog dialog;
    Master master;
    AlertDialog.Builder builder;
    AdminDetails adminDetails;

    String number, pwd, response, pwd1, pwd2, otp_check, versionName;
    long session;
    int forgotOtpDialogId;
    JSONObject resObj;

    Boolean login;
    EditText eMobileNumber, ePassword, eOTP, ePass1, ePass2;
    Button bLogin, bForgotPassword, bOTP, bConfirm, bCancel;
    CheckBox chkBox;

   // private BroadcastReceiver mRegistrationBroadcastReceiver;
    private static final String TAG = "MainActivityAdminApp";

    @Override
    protected void onResume() {
        super.onResume();
        Master.getAdminData(getApplicationContext());
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = sharedPreferences.edit();

        if(Master.isNetworkAvailable(MainActivity.this))
        {
            PackageInfo pInfo = null;
            try {
                pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            }
            catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            //versionName = pInfo.versionName;
            versionName = pInfo.versionCode + "";
            new CheckApkVersionTask(MainActivity.this).execute(versionName);
        }
        else
        {
            if(sharedPreferences.getInt("versionchk", -1)==-1 || sharedPreferences.getInt("versionchk", -1) == 0)
                initialize();
            else if(sharedPreferences.getInt("versionchk", -1)==1)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                builder.setMessage(R.string.label_alertdialog_app_update_msg)
                        .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                initialize();
                            }
                        }).setCancelable(false);
                dialog = builder.show();
            }
            else
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage(R.string.label_alertdialog_app_update_msg)
                        .setNegativeButton(R.string.label_alertdialog_close, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        }).setCancelable(false);

                dialog = builder.show();
            }
        }
    }

    void initialize()
    {
        login = sharedPreferences.getBoolean("login", false);
        master = new Master();
        if (login)
        {
            AdminDetails.setEmail(sharedPreferences.getString("emailid", "abc@def.com"));
            AdminDetails.setMobileNumber((sharedPreferences.getString("mobilenumber", "0000000000")));
            AdminDetails.setPassword(sharedPreferences.getString("password", "null"));
            AdminDetails.setAbbr(sharedPreferences.getString("abbr", "Abbr"));
            AdminDetails.setID(sharedPreferences.getString("org_id", "Org_ID"));
            AdminDetails.setName(sharedPreferences.getString("name", "Name"));

            startActivity(new Intent(MainActivity.this, DashboardActivity.class));
            //startActivity(new Intent(MainActivity.this, EditProductActivity.class));
            finish();
        }
        else
        {
            setContentView(R.layout.activity_main);
            eMobileNumber = (EditText) findViewById(R.id.eMobileNumber);
            ePassword = (EditText) findViewById(R.id.ePassword);
            ePassword.setLongClickable(false);
            chkBox = (CheckBox) findViewById(R.id.chkBox);

            //fetching from local database
            if((!sharedPreferences.getString("mobilenumber","").equals("")&&!sharedPreferences.getString("password","").equals("")))
            {
                eMobileNumber.setText(sharedPreferences.getString("mobilenumber", ""));
                ePassword.setText(sharedPreferences.getString("password",""));
            }

            bLogin = (Button) findViewById(R.id.bLogin);
            bLogin.setOnClickListener(this);

            bForgotPassword= (Button) findViewById(R.id.bForgotPassword);
            bForgotPassword.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.bLogin :
                login();
                break;

            case R.id.bForgotPassword :
                forgot();
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        finish();
    }

    public void login()
    {
        number = eMobileNumber.getText().toString();
        pwd = ePassword.getText().toString();

        if(number.equals("") && pwd.equals(""))
        {
            Toast.makeText(MainActivity.this,R.string.label_toast_Please_enter_a_mobile_number_and_a_password, Toast.LENGTH_SHORT).show();
        }
        else if(number.equals(""))
        {
            Toast.makeText(MainActivity.this,R.string.label_toast_Please_enter_a_mobile_number, Toast.LENGTH_SHORT).show();
        }
        else if(pwd.equals(""))
        {
            Toast.makeText(MainActivity.this, R.string.label_toast_Please_enter_password, Toast.LENGTH_SHORT).show();
        }
        else if(number.length()!= 10)
        {
            Toast.makeText(getApplicationContext(), R.string.label_toast_enter_valid_number, Toast.LENGTH_SHORT).show();
        }
        else if(Master.isNetworkAvailable(MainActivity.this))
        {
            JSONObject obj = new JSONObject();
            try
            {
                obj.put("phonenumber","91"+number);
                obj.put("password",pwd);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            loginVerification = new LoginVerification(MainActivity.this);
            loginVerification.execute(obj);
        }
        else
        {
            Toast.makeText(getApplicationContext(),R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
        }
    }





    public void forgot()   {

        number = eMobileNumber.getText().toString();

        if(!number.equals(""))
        {
            if(Master.isNetworkAvailable(MainActivity.this))
            {

                /*builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Forgot Password");*/
                dialog=new Dialog(MainActivity.this);
                dialog.setContentView(R.layout.new_password_box);
                dialog.setTitle(R.string.dialog_label_member_verification);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);

                eOTP = (EditText) dialog.findViewById(R.id.eOTP);
                eOTP.setVisibility(View.GONE);

                ePass1 = (EditText) dialog.findViewById(R.id.ePass1);
                ePass1.setVisibility(View.GONE);

                ePass2 = (EditText) dialog.findViewById(R.id.ePass2);
                ePass2.setVisibility(View.GONE);

                bConfirm = (Button) dialog.findViewById(R.id.bConfirm);
                bConfirm.setVisibility(View.GONE);

                bOTP = (Button) dialog.findViewById(R.id.bOTP);
                bCancel = (Button) dialog.findViewById(R.id.bCancel);

                bCancel.setOnClickListener(new View.OnClickListener()
                                           {
                                                  @Override
                                                  public void onClick(View v)
                                                  {
                                                      dialog.dismiss();
                                                  }
                                              }
                );

                bOTP.setOnClickListener(new View.OnClickListener()
                                           {
                                               @Override
                                               public void onClick(View v)
                                               {
                                                   JSONObject obj = new JSONObject();
                                                   try
                                                   {
                                                       obj.put("phonenumber","91"+number);
                                                       anp = new GetOTPTask(MainActivity.this);
                                                       anp.execute(obj);
                                                   }
                                                   catch (JSONException e)
                                                   {
                                                       e.printStackTrace();
                                                   }
                                               }
                                         }
                );

                bConfirm.setOnClickListener(new View.OnClickListener()
                                               {
                                                   @Override
                                                   public void onClick(View v)
                                                   {

                                                       long session2 = System.currentTimeMillis();
                                                       if(session2<session)
                                                       {
                                                           if(eOTP.getText().toString().equals(otp_check))
                                                           {
                                                               countDownTimer.cancel();
                                                               pwd1 = ePass1.getText().toString();
                                                               pwd2 = ePass2.getText().toString();

                                                               if(!pwd1.equals(""))
                                                               {
                                                                   if(pwd1.equals(pwd2)){
                                                                       try
                                                                       {
                                                                           JSONObject addPasswordDB= new JSONObject();
                                                                           addPasswordDB.put("phonenumber","91"+number);
                                                                           addPasswordDB.put("password", pwd1);
                                                                           if(Master.isNetworkAvailable(MainActivity.this))
                                                                           {
                                                                               addPasswordTask = new AddPasswordTask(MainActivity.this);
                                                                               addPasswordTask.execute(addPasswordDB);
                                                                           }
                                                                           else
                                                                           {
                                                                               Toast.makeText(getApplicationContext(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
                                                                           }
                                                                       }
                                                                       catch(JSONException e)
                                                                       {
                                                                           e.printStackTrace();
                                                                       }
                                                                   }
                                                                   else
                                                                   {
                                                                       ePass1.setText("");
                                                                       ePass2.setText("");
                                                                       Toast.makeText(getApplicationContext(),R.string.label_toast_Passwords_do_not_match, Toast.LENGTH_SHORT).show();
                                                                   }
                                                               }
                                                               else
                                                               {
                                                                   ePass1.setText("");
                                                                   ePass2.setText("");
                                                                   Toast.makeText(getApplicationContext(),R.string.label_toast_Please_enter_password, Toast.LENGTH_SHORT).show();
                                                               }
                                                           }
                                                           else
                                                           {
                                                               eOTP.setHint(R.string.hint_enter_otp_received);
                                                               Toast.makeText(getApplicationContext(), R.string.label_toast_Please_enter_correct_OTP, Toast.LENGTH_SHORT).show();

                                                           }
                                                       }
                                                       else
                                                       {
                                                           eOTP.setText("");
                                                           eOTP.setHint(R.string.hint_enter_otp_received);
                                                           eOTP.setEnabled(false);

                                                           ePass1.setText("");
                                                           ePass1.setHint(R.string.hint_enter_new_password);
                                                           ePass1.setEnabled(false);

                                                           ePass2.setText("");
                                                           ePass2.setHint(R.string.hint_reenter_new_password);
                                                           ePass2.setEnabled(false);

                                                           bOTP.setEnabled(true);
                                                           bConfirm.setVisibility(View.GONE);
                                                           bConfirm.setEnabled(false);
                                                           bCancel.setVisibility(View.VISIBLE);
                                                           bOTP.setVisibility(View.VISIBLE);

                                                           Toast.makeText(getApplicationContext(),R.string.label_toast_OTP_expired, Toast.LENGTH_SHORT).show();
                                                       }
                                                   }
                                               }
                );
                dialog.show();
            }
            else
            {
                Toast.makeText(this,R.string.label_toast_Please_check_internet_connection,Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Toast.makeText(this,R.string.label_toast_Please_enter_mobile_number,Toast.LENGTH_SHORT).show();
        }
    }


    public class GetOTPTask extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        Context context;

        public GetOTPTask(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {

            String url = master.getForgotPasswordURL();
            GetJSON getJson = new GetJSON();
            response = getJson.getJSONFromUrl(url,params[0],"POST",false,null,null);
            return response;
        }

        @Override
        protected void onPostExecute(String response2) {

            pd.dismiss();

            if(response2.equals("exception"))
            {
                Master.alertDialog(MainActivity.this, getString(R.string.label_cannot_connect_to_the_server), "OK");
            }
            else
            {

                try {
                    JSONObject obj = new JSONObject(response2);

                    otp_check = obj.getString("otp");

                    if(otp_check.equals("null")){
                        Toast.makeText(getApplicationContext(), R.string.label_toast_You_are_not_a_registered_member, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(getApplicationContext(),R.string.label_toast_OTP_sent_to_email,Toast.LENGTH_SHORT).show();
                        dialog.setTitle(R.string.dialog_title_set_new_password);
                        eOTP.setVisibility(View.VISIBLE);
                        eOTP.setEnabled(true);
                        ePass1.setVisibility(View.VISIBLE);
                        ePass2.setVisibility(View.VISIBLE);
                        ePass1.setEnabled(true);
                        ePass2.setEnabled(true);
                        bOTP.setVisibility(View.GONE);
                        bConfirm.setVisibility(View.VISIBLE);
                        bConfirm.setEnabled(true);
                        bCancel.setVisibility(View.VISIBLE);
                        session = System.currentTimeMillis();
                        forgotOtpDialogId=2;
                        countDownTimer = new OTPCountDownTimer(600000, 1000);
                        countDownTimer.start();
                        session = session + 600000;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public class  AddPasswordTask extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        Context context;

        public AddPasswordTask(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {

            String url = master.getChangePasswordURL();

            GetJSON getJson = new GetJSON();
            response = getJson.getJSONFromUrl(url,params[0],"POST",false,null,null);
            return response;
        }


        @Override
        protected void onPostExecute(String response3) {
            if(pd != null && pd.isShowing())
                pd.dismiss();


            if(response3.equals("exception"))
            {
                Master.alertDialog(MainActivity.this, getString(R.string.label_cannot_connect_to_the_server), "OK");
            }
            else
            {
                try {

                    JSONObject obj = new JSONObject(response3);
                    String msg = obj.getString("Status");

                    if(msg.equals("Success")) {
                        Toast.makeText(getApplicationContext(),R.string.label_toast_Password_successfully_changed, Toast.LENGTH_SHORT).show();

                    }
                    else {
                        Toast.makeText(getApplicationContext(),R.string.label_toast_Sorry_please_try_again_later, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    dialog.dismiss();
                }
                catch(JSONException e){
                    e.printStackTrace();
                }
            }
        }
    }



    public class OTPCountDownTimer extends CountDownTimer
    {

        public OTPCountDownTimer(long startTime, long interval)
        {
            super(startTime, interval);
        }

        @Override
        public void onFinish()
        {
            eOTP.setText("");
            eOTP.setHint(R.string.hint_enter_otp_received);
            eOTP.setEnabled(false);
            bCancel.setEnabled(true);
            bCancel.setVisibility(View.VISIBLE);
            bOTP.setEnabled(true);
            bOTP.setVisibility(View.VISIBLE);

            ePass1.setText("");
            ePass1.setHint(R.string.hint_enter_new_password);
            ePass1.setEnabled(true);

            ePass2.setText("");
            ePass2.setHint(R.string.hint_reenter_new_password);
            ePass2.setEnabled(true);

            ePass1.setVisibility(View.VISIBLE);
            ePass2.setVisibility(View.VISIBLE);

            bConfirm.setEnabled(false);
            bConfirm.setVisibility(View.GONE);

            Toast.makeText(getApplicationContext(),R.string.label_toast_OTP_expired, Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onTick(long millisUntilFinished)
        {
        }
    }

    public class LoginVerification extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        Context context;

        public LoginVerification(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {

            GetJSON getJson = new GetJSON();
            response = getJson.getJSONFromUrl(master.getLoginURL(),params[0],"POST",false,null,null);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            if(pd != null && pd.isShowing())
                pd.dismiss();
            if (response.equals("exception"))
            {
                Master.alertDialog(MainActivity.this, getString(R.string.label_cannot_connect_to_the_server), "OK");
            }
            else
            {
                try
                {
                    resObj = new JSONObject(response);
                    if (resObj.get("Authentication").toString().equals("success")) {
                        adminDetails.setMobileNumber(number);
                        adminDetails.setPassword(pwd);

                        try {

                            String emailId = resObj.getString("email");
                            adminDetails.setEmail(emailId);

                            JSONObject jsonObject = resObj.getJSONObject("organization");

                            adminDetails.setAbbr(jsonObject.getString("abbr"));
                            adminDetails.setID(jsonObject.getString("org_id"));
                            adminDetails.setName(jsonObject.getString("name"));

                            editor.putBoolean("login", true);
                            if(chkBox.isChecked())
                            {
                                editor.putString("signchk", "true");
                                editor.putString("emailid",emailId);
                                editor.putString("password",pwd);
                                editor.putString("mobilenumber",number);
                                editor.putString("name", AdminDetails.getName());
                                editor.putString("abbr", AdminDetails.getAbbr());
                                editor.putString("org_id", AdminDetails.getID());
                            }
                            else
                            {
                                editor.putString("signchk", "false");
                                editor.putString("mobilenumber","");
                                editor.putString("password","");
                            }
                            editor.commit();
//                            Intent intent = new Intent(MainActivity.this, RegistrationIntentService.class);
//                            if (checkPlayServices()) {
//                                startService(intent);
//                            }
                            Intent i = new Intent(MainActivity.this, DashboardActivity.class);
                            startActivityForResult(i, 0);
                            finish();

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }


                    } else {

                        if(resObj.get("registered").toString().equals("false"))
                        {
                            Toast.makeText(getApplicationContext(),R.string.label_toast_You_are_not_a_registered_member, Toast.LENGTH_SHORT).show();
                            eMobileNumber.setText("");
                            ePassword.setText("");
                        }
                        else
                        {
                            String error =resObj.get("Error").toString();
                            if(error.equals("null"))
                            {
                                Toast.makeText(getApplicationContext(), R.string.label_incorrect_password, Toast.LENGTH_SHORT).show();
                                ePassword.setText("");
                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                }
            }
        }
    }

    public class  CheckApkVersionTask extends AsyncTask<String, String, String> {

        ProgressDialog pd;
        Context context;

        public CheckApkVersionTask(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.pd_checking_for_updates));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            GetJSON jParser = new GetJSON();
            //System.out.println("Chk apk JSON: " + params[0]);
            //System.out.println("Chk apk URL: " + Master.getCheckApkURL(params[0]));
            response = jParser.getJSONFromUrl(Master.getCheckApkURL(params[0]), null, "GET", true, null, null);
            //System.out.println("Chk apk response: " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String response3) {
            if(pd != null && pd.isShowing())
                pd.dismiss();

            if (response3.equals("exception"))
            {
                editor.putInt("versionchk",-1);
                editor.commit();
                initialize();
            }
            else
            {
                try {
                    JSONObject obj = new JSONObject(response3);
                    String msg = obj.getString("response");
                    if(msg.equals("0"))
                    {
                        editor.putInt("versionchk", 0);
                        editor.commit();
                        initialize();
                    }

                    if(msg.equals("1"))
                    {
                        editor.putInt("versionchk", 1);
                        editor.commit();
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setMessage(R.string.label_alertdialog_app_update_msg)
                                .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        initialize();
                                    }
                                }).setCancelable(false);
                        dialog = builder.show();
                    } else {

                        if(msg.equals("2")) {

                            editor.putInt("versionchk",2);
                            editor.commit();

                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                            builder.setMessage(R.string.label_alertdialog_app_update_msg)
                                    .setPositiveButton(R.string.label_alertdialog_close, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            finish();
                                        }
                                    }).setCancelable(false);
                            dialog = builder.show();
                        }
                    }
                }
                catch(JSONException e){
                    e.printStackTrace();
                }
            }
        }
    }


}
