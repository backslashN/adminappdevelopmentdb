package admin.lokacart.ict.mobile.com.adminapp;

/**
 * Created by Vishesh on 29/12/15.
 */

import android.animation.ValueAnimator;
import android.app.AlertDialog;

import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;

import admin.lokacart.ict.mobile.com.adminapp.containers.Product;
import admin.lokacart.ict.mobile.com.adminapp.containers.ProductType;

public class Master
{
    public static ArrayList<String> productList, quantityList, priceList,descList;
    public static ArrayList<ProductType>productTypeList;
    public static ArrayList<Product> productNameList;//Contains the list of the products in a product type


    public static final int productTypeClickKey = 1;
    public static final int existingUserClickKey = 5;
    public static final int pendingUserRequestKey = 6;
    public static final int savedToProcessedKey = 2;
    public static final int cancelledOrderKey = 4;
    public static final int processedOrderKey = 3;
    public static final int deliveredOrderKey = 7;
    public static final int productClickKey = 0;
    public static final String  FAQ_TAG = "faq_fragment";


    public static int fabClickKey=0; // to check for the floating action bar
    public static int backPress=0; // to check whether back button has been presssed once
    public static int backCheck=0;

    public static String token;
    public static String getToken()
    {
        return token;
    }
    public static void setToken(String tok)
    {
        token = tok;
    }

//******************functions to get index of Navigation menu items*******************************
    public static int getDashboardTAG()
    {
        return 0;
    }
    public static int getProductTAG()
    {
        return 1;
    }
    public static int getOrdersTAG()
    {
        return 2;
    }
    public static int getMembersTAG()
    {
        return 3;
    }
    public static int getSettingsTAG()
    {
        return 4;
    }
    public static int getLogoutTAG()
    {
        return 5;
    }

//--------------------------End of Navigation item functions----------------------------------------

    //public static final String serverURL = "http://ruralict.cse.iitb.ac.in/RuralIvrs/";
    //public static final String serverURL = "http://192.168.1.131:8080/";
    public static final String serverURL = "http://ruralict.cse.iitb.ac.in/ruralict/";

    public static String getBroadcastURL() { return  serverURL + "api/sendbroadcast"; }
    public static String getLoginURL()
    {
        return serverURL+"app/loginAdmin";
    }
    public static String getChangePasswordURL()
    {
        return serverURL + "app/changepassword";
    }
    public static String getForgotPasswordURL() { return serverURL + "app/forgotpassword"; }
    public static String getAdminDetailsAndProductTypeURL() { return serverURL + "api/products/search/byType/mapnew?orgabbr=";}
    public static String getAddNewProductTypeUrl()
    {
        return serverURL + "api/producttype/add";
    }
    public static String getEditProductTypeURL()
    {
        return serverURL + "api/producttype/edit";
    }
    public static String getDeleteProductTypeURL()
    {
        return serverURL + "api/producttype/delete";
    }
    public static String getSaveProductTypeURL() { return serverURL + "api/setsequence"; }
    public static String getProductURL()
    {
        return serverURL + "api/products/search/byType/map?orgabbr=";
    }
    public static String getAddNewProductURL()
    {
        return serverURL + "api/product/add";
    }
    public static String getEditProductURL()
    {
        return serverURL + "api/product/edit";
    }
    public static String getDeleteProductURL()
    {
        return serverURL + "api/product/delete";
    }
    public static String getDashboardDetailsURL()
    {
        return serverURL + "api/dashboard?orgabbr=";
    }
    public static String getDeregisterTokenURL()
    {
        return serverURL + "/app/deregistertoken";
    }
    public static String getSavedOrderURL()
    {
        return serverURL + "api/orders/saved";
    }
    public static String getProcessedOrderURL()
    {
        return serverURL + "api/orders/processed";
    }
    public static String getCancelledOrderURL()
    {
        return serverURL + "api/orders/cancelled";
    }
    public static String getChangeSavedToProcessedOrderURL()
    {
        return serverURL + "/api/orders/changestate/processed/";
    }
    public static String getExistingUsersURL(String orgAbbr)
    {
        return serverURL + "/api/" + orgAbbr + "/manageUsers/userList";
    }
    public static String getPendingRequestsURL(String orgAbbr)
    {
        return serverURL + "/api/" + orgAbbr + "/manageUsers/userApprovalList";
    }

    public static String getDeliveredOrderList(String orgAbbr){
        return serverURL + "/api/orders/delivered?orgabbr=" + orgAbbr;
    }

    public static String getChangePaidState(int orderId){
        return serverURL + "api/orders/paid/" + orderId;
    }

    public static String getChangeDeliveredState(int orderId){
        return serverURL + "api/orders/update/" + orderId;
    }

    public static String getEditUserURL(String orgAbbr)
    {
        return serverURL + "/api/" + orgAbbr + "/manageUsers/editUser";
    }
    public static String getAddUserURL() { return serverURL + "/app/refer" ;}
    public static String getApproveURL() { return serverURL + "/app/approve";}
    public static String getDeleteUser() { return serverURL + "/app/delete";}
    public static String getRejectURL()
    {
        return serverURL + "/app/reject";
    }
    public static String getGeneralSettingsURL(String orgAbbr, String number)
    {
        return serverURL + "/api/" + orgAbbr + "/appsettingsview?number=" + number;
    }
    public static String getThresholdUpdate(String orgAbbr){
        return serverURL + "/api/" + orgAbbr + "/thresholdupdate";
    }
    public static String getGeneralSettingsUpdateURL(String orgAbbr)
    {
        return serverURL + "/api/" + orgAbbr + "/appsettingsupdate";
    }

    public static String getGeneralSettingsOrganisationDescriptionURL(String orgAbbr)
    {
        return serverURL + "/api/" + orgAbbr + "/orgdesc";
    }
    public static String getGeneralSettingsLogoUploadURL(String orgAbbr)
    {
        return serverURL + "api/" + orgAbbr + "/logoupload";
    }
    public static String getCheckApkURL(String versionNumber)
    {
        return serverURL + "/app/versioncheckadmin?version=" + versionNumber;
    }
    public static String getUploadImageURL(String orgabbr, String product)
    {
        return serverURL + "api/uploadpicture?orgabbr=" + orgabbr + "&productName=" + product;
    }
    public static String getUploadAudioURL(String orgabbr, String product)
    {
        return serverURL + "api/uploadaudio?orgabbr=" + orgabbr + "&productName=" + product;
    }

    public static boolean isNetworkAvailable(Context context)
    {
        ConnectivityManager connectivityManager;
        connectivityManager = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return (activeNetworkInfo != null && activeNetworkInfo.isConnected());
    }

    public static void alertDialog(Context context, String message, String button_text) //for showing the messages on an alertbox
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage(message);
        builder.setCancelable(true);

        builder.setPositiveButton(
                button_text,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder.create();
        alert11.show();
    }

    public static void getAdminData(Context context)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        AdminDetails.setEmail(sharedPreferences.getString("emailid", "abc@def.com"));
        AdminDetails.setMobileNumber((sharedPreferences.getString("mobilenumber", "0000000000")));
        AdminDetails.setPassword(sharedPreferences.getString("password", "null"));
        AdminDetails.setAbbr(sharedPreferences.getString("abbr", "Abbr"));
        AdminDetails.setID(sharedPreferences.getString("org_id", "Org_ID"));
        AdminDetails.setName(sharedPreferences.getString("name", "Name"));
    }

    public static DialogPlus dialogPlus(ViewHolder viewHolder, Context context)
    {
        DialogPlus dialog = DialogPlus.newDialog(context)
                .setContentHolder(viewHolder)
                .setGravity(Gravity.CENTER)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setExpanded(false)
                .setCancelable(false)
                .create();
        dialog.show();
        return dialog;
    }

    public static final String IMAGE_FILE_TYPE = "image/jpg";
    public static final String AUDIO_FILE_TYPE = "audio/mp3";

    public static String okhttpUpload(File file, String serverURL, String fileType, String email, String password) {

        OkHttpClient client = new OkHttpClient();
//        File file= new File (path);
        JSONObject jsonObject;


            RequestBody requestBody = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addFormDataPart("file", file.getName(),
                            RequestBody.create(MediaType.parse(fileType), file))
                    .build();

            Request request = new Request.Builder()
                    .url(serverURL)
                    .post(requestBody)
                    .addHeader("authorization", "Basic " + new String(Base64.encode((email + ":" + password).getBytes(), Base64.NO_WRAP)))
                    .build();

            try
            {
                Response response = client.newCall(request).execute();
                String resp = response.body().string();
                return resp;

            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } catch (Exception ex)
            {
                return null;
            }
    }




    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @author paulburke
     */
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    //------------function to read an image and scale it down--------------------------------
    //---------------------------------------------------------------------------------------
    public static Bitmap readBitmap(Uri selectedImage, Context context, int scalValue) {
        Bitmap bm = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = scalValue;
        AssetFileDescriptor fileDescriptor =null;
        try {
            fileDescriptor = context.getContentResolver().openAssetFileDescriptor(selectedImage, "r");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        finally{
            try {
                bm = BitmapFactory.decodeFileDescriptor(fileDescriptor.getFileDescriptor(), null, options);
                fileDescriptor.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bm;
    }
    //***************************************************************************************

    //----------------function to get image rotation-----------------------------------------
    //---------------------------------------------------------------------------------------
    public static int getImageOrientation(String imagePath){
        int rotate = 0;
        try {

            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(
                    imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rotate;
    }
    //***************************************************************************************


    //----------------function to clear a bitmap from memory---------------------------------
    //---------------------------------------------------------------------------------------
    public static void clearBitmap(Bitmap bm) {
        bm.recycle();
        System.gc();
    }
    //***************************************************************************************

    public static void rotateImage(ImageView imageView, int angle, int pivotX, int pivotY)
    {
        Matrix matrix = new Matrix();
        imageView.setScaleType(ImageView.ScaleType.MATRIX);   //required
        matrix.postRotate((float) angle, pivotX, pivotY);
        imageView.setImageMatrix(matrix);
    }

    public static void copyFile(File src, File dst) throws IOException {
        FileInputStream inStream = new FileInputStream(src);
        FileOutputStream outStream = new FileOutputStream(dst);
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();
        inChannel.transferTo(0, inChannel.size(), outChannel);
        inStream.close();
        outStream.close();

    }

}
